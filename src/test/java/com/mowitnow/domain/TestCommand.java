package com.mowitnow.domain;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.mowitnom.domain.Command;
import com.mowitnom.exception.InvalidCommandException;

public class TestCommand {

	@Test(expected = InvalidCommandException.class)
	public void should_exception_when_command_is_invalid() throws InvalidCommandException{
		char invalidCommand = 'Z';
		Command.getCommand(invalidCommand);
	}
	
	@Test
	public void should_return_command_when_string_command_is_valid() throws InvalidCommandException{
		char validCommand = Command.A.name().charAt(0);
		assertTrue(Command.getCommand(validCommand) instanceof Command);
	}
}
