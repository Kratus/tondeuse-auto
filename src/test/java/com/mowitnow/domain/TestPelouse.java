package com.mowitnow.domain;

import org.junit.Test;

import com.mowitnom.domain.Coordonnee;
import com.mowitnom.domain.Dimension;
import com.mowitnom.domain.Orientation;
import com.mowitnom.domain.Pelouse;
import com.mowitnom.exception.OutOfPelouseException;

public class TestPelouse {

	@Test(expected = OutOfPelouseException.class)
	public void should_return_exception_when_X_is_out_of_max_bounds() throws OutOfPelouseException {
		Pelouse pelouse = new Pelouse(new Dimension(new Integer(5), new Integer(5)));
		pelouse.isValidCoordonnee(new Coordonnee(new Integer(6), new Integer(5), Orientation.E));
	}
	
	@Test(expected = OutOfPelouseException.class)
	public void should_return_exception_when_X_is_out_of_min_bounds() throws OutOfPelouseException {
		Pelouse pelouse = new Pelouse(new Dimension(new Integer(5), new Integer(5)));
		pelouse.isValidCoordonnee(new Coordonnee(new Integer(-1), new Integer(5), Orientation.E));
	}
	
	@Test(expected = OutOfPelouseException.class)
	public void should_return_exception_when_Y_is_out_of_max_bounds() throws OutOfPelouseException {
		Pelouse pelouse = new Pelouse(new Dimension(new Integer(5), new Integer(5)));
		pelouse.isValidCoordonnee(new Coordonnee(new Integer(3), new Integer(7), Orientation.E));
	}
	
	@Test(expected = OutOfPelouseException.class)
	public void should_return_exception_when_Y_is_out_of_min_bounds() throws OutOfPelouseException {
		Pelouse pelouse = new Pelouse(new Dimension(new Integer(5), new Integer(5)));
		pelouse.isValidCoordonnee(new Coordonnee(new Integer(3), new Integer(-1), Orientation.E));
	}
}
