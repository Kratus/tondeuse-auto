package com.mowitnow;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.mowitnom.MainApplication;


public class TestMainApplication {
	
	String pelouseDimension;
	String tondeuseInitPosition;
	String commands;
	
	@Test
	public void testEntryPointTondeuseUne() {
		pelouseDimension = "5 5";
		tondeuseInitPosition = "1 2 N";
		commands = "GAGAGAGAA";
		
		String finalPosition = MainApplication.entryPoint(pelouseDimension, tondeuseInitPosition, commands);
		assertEquals("1 3 N", finalPosition);
	}
	
	@Test
	public void testEntryPointTondeuseDeux() {
		pelouseDimension = "5 5";
		tondeuseInitPosition = "3 3 E";
		commands = "AADAADADDA";
		
		String finalPosition = MainApplication.entryPoint(pelouseDimension, tondeuseInitPosition, commands);
		assertEquals("5 1 E", finalPosition);
	}
}
