package com.mowitnom;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mowitnom.domain.Command;
import com.mowitnom.domain.Coordonnee;
import com.mowitnom.domain.Dimension;
import com.mowitnom.domain.Orientation;
import com.mowitnom.domain.Pelouse;
import com.mowitnom.domain.Tondeuse;
import com.mowitnom.exception.InvalidCommandException;
import com.mowitnom.exception.OutOfPelouseException;

public class MainApplication {
	
	private static final Logger LOG = LoggerFactory.getLogger(MainApplication.class);
	
	private static final String SEPARATOR = " ";
	
	public static String entryPoint(String pelouseDimension, String tondeuseInitPosition, String commands){
		
		// Instantiation d'une nouvelle pelouse
		Integer maxX = Integer.parseInt(pelouseDimension.split(SEPARATOR)[0]);
		Integer maxY = Integer.parseInt(pelouseDimension.split(SEPARATOR)[1]);
		Pelouse pelouse = new Pelouse(new Dimension(maxX, maxY));
		
		// Instantiation d'une nouvelle tondeuse
		Integer initX = Integer.parseInt(tondeuseInitPosition.split(SEPARATOR)[0]);
		Integer initY = Integer.parseInt(tondeuseInitPosition.split(SEPARATOR)[1]);
		Orientation initOrientation = Orientation.getOrientation(tondeuseInitPosition.split(SEPARATOR)[2]);
		Coordonnee initPosition = new Coordonnee(initX, initY, initOrientation);
		Tondeuse tondeuse = new Tondeuse(initPosition);
		
		// Lancement de la séquence de commande
		for(int i=0; i<commands.length(); i++){
			char strCommand = commands.charAt(i);
			
			LOG.info("NEXT COMMAND : " + strCommand);
			
			Command command;
			try {
				command = Command.getCommand(strCommand);
				command.applyCommand(tondeuse, pelouse);
			} catch (InvalidCommandException e) {
				LOG.info("Attention Commande Invalide");
			} catch (OutOfPelouseException e) {
				LOG.info("L'avancement de la tondeuse n'est pas possible, car en dehors de la pelouse");
			}
		}
		return tondeuse.communicatePostion();
	}
}
