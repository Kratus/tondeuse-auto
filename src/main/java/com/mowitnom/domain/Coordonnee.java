package com.mowitnom.domain;

/**
 * La position de la tondeuse est représentée par une combinaison de coordonnées (x,y) et d'une
l* ettre indiquant l'orientation selon la notation cardinale anglaise (N,E,W,S).

 * @author Kratus
 *
 */
public class Coordonnee {
	
	private Integer x;
	
	private Integer y;
	
	private Orientation orientation;
	
	public Coordonnee(Integer x, Integer y, Orientation orientation) {
		super();
		this.x = x;
		this.y = y;
		this.orientation = orientation;
	}

	public Integer getX() {
		return x;
	}

	public void setX(Integer x) {
		this.x = x;
	}

	public Integer getY() {
		return y;
	}

	public void setY(Integer y) {
		this.y = y;
	}

	public Orientation getOrientation() {
		return orientation;
	}

	public void setOrientation(Orientation orientation) {
		this.orientation = orientation;
	}
	
	/**
	 * Methode donnant la coordonnée suivante
	 * en prenant en compte l'orientation
	 * 
	 * @return
	 */
	public Coordonnee nextCoordonnee(){
		
		switch(getOrientation()){
			
			case N:
				return new Coordonnee(getX(), getY()+1, getOrientation());
			case S:
				return new Coordonnee(getX(), getY()-1, getOrientation());
			case E:
				return new Coordonnee(getX()+1, getY(), getOrientation());
			case W:
				return new Coordonnee(getX()-1, getY(), getOrientation());
			default:
				return null;
		}
	}
	
	/**
	 * Application d'une rotation à droite de 90°
	 */
	public void applyRightRotation() {
		this.orientation = this.orientation.applyRightRotation();
	}
	
	/**
	 * Application d'une rotation à gauche de 90°
	 */
	public void applyLeftRotation() {
		this.orientation = this.orientation.applyLeftRotation();
	}
	
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		
		str.append(getX()).append(" ")
			.append(getY()).append(" ")
			.append(getOrientation().name());
		
		return str.toString();
	}
}
