package com.mowitnom.domain;

import com.mowitnom.exception.OutOfPelouseException;

public class Dimension {

	private Integer maxX;
	
	private Integer maxY;
	
	private Integer minX;
	
	private Integer minY;
	
	public Dimension(Integer maxX, Integer maxY) {
		super();
		this.maxX = maxX;
		this.maxY = maxY;
		this.minX = new Integer(0);
		this.minY = new Integer(0);
	}

	public Integer getMaxX() {
		return maxX;
	}

	public void setMaxX(Integer maxX) {
		this.maxX = maxX;
	}

	public Integer getMaxY() {
		return maxY;
	}

	public void setMaxY(Integer maxY) {
		this.maxY = maxY;
	}

	public Integer getMinX() {
		return minX;
	}

	public void setMinX(Integer minX) {
		this.minX = minX;
	}

	public Integer getMinY() {
		return minY;
	}

	public void setMinY(Integer minY) {
		this.minY = minY;
	}
}
