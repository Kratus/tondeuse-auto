package com.mowitnom.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *  Tondeuse à gazon automatique, destinée aux
 *  surfaces rectangulaires.
 *  
 * @author Kratus
 *
 */
public class Tondeuse {
	
	private final Logger LOG = LoggerFactory.getLogger(Tondeuse.class);
	
	private Coordonnee position;
	
	public Tondeuse(Coordonnee position) {
		super();
		this.position = position;
	}

	public Coordonnee getPosition() {
		return position;
	}

	public void applyCoordonnee(Coordonnee nextCoordonnee) {
		position = nextCoordonnee;
	}
	
	/**
	 * Méthode permettant de communiquer la position actuelle 
	 * de la tondeuse
	 * 
	 * @return
	 */
	public String communicatePostion(){
		LOG.info("Communication de ma position : " + position.toString());
		return position.toString();
	}
}
