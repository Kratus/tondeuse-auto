package com.mowitnom.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mowitnom.exception.OutOfPelouseException;

public class Pelouse {
	
	private static final Logger LOG = LoggerFactory.getLogger(Pelouse.class);
	
	private Dimension dimension;

	public Pelouse(Dimension dimension) {
		super();
		this.dimension = dimension;
	}

	public Dimension getDimension() {
		return dimension;
	}

	public void setDimension(Dimension dimension) {
		this.dimension = dimension;
	}
	
	// METHOD
	public void isValidCoordonnee(Coordonnee coordonnee) throws OutOfPelouseException {
		if(coordonnee.getX() > dimension.getMaxX() || coordonnee.getY() > dimension.getMaxY()
				|| coordonnee.getX() < dimension.getMinX() || coordonnee.getY() < dimension.getMinY()){
			throw new OutOfPelouseException();
		}
		LOG.info("NEXT COORDONNEE POSSIBLE");
	}
}
