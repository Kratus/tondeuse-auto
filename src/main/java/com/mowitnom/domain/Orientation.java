package com.mowitnom.domain;

import java.util.EnumSet;

/**
 * Notation cardinale anglaise (N,E,W,S)
 */
public enum Orientation {
	N {
		@Override
		public Orientation applyLeftRotation() {
			return Orientation.W;
		}

		@Override
		public Orientation applyRightRotation() {
			return Orientation.E;
		}
	}, E {
		@Override
		public Orientation applyLeftRotation() {
			return Orientation.N;
		}

		@Override
		public Orientation applyRightRotation() {
			return Orientation.S;
		}
	}, W {
		@Override
		public Orientation applyLeftRotation() {
			return Orientation.S;
		}

		@Override
		public Orientation applyRightRotation() {
			return Orientation.N;
		}
	}, S {
		@Override
		public Orientation applyLeftRotation() {
			return Orientation.E;
		}

		@Override
		public Orientation applyRightRotation() {
			return Orientation.W;
		}
	};
	
	// Application d'une rotation à gauche de 90°
	public abstract Orientation applyLeftRotation();
	
	// Application d'une rotation à droite de 90°
	public abstract Orientation applyRightRotation();
	
	public static Orientation getOrientation(String strOrientation) {
		for(Orientation orientation: EnumSet.allOf(Orientation.class)){
			if(orientation.name().equals(strOrientation))
				return orientation;
		}
		return Orientation.N; // Valeur par defaut 
	}
}
