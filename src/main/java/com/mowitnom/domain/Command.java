package com.mowitnom.domain;

import java.util.EnumSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mowitnom.exception.InvalidCommandException;
import com.mowitnom.exception.OutOfPelouseException;

/**
 * Command disponible pour piloter une tondeuse
 */
public enum Command {
	
	D {
		@Override
		public void applyCommand(Tondeuse tondeuse, Pelouse pelouse) {
			tondeuse.getPosition().applyRightRotation();
		}
	}, G {
		@Override
		public void applyCommand(Tondeuse tondeuse, Pelouse pelouse) {
			tondeuse.getPosition().applyLeftRotation();
		}
	}, A {
		@Override
		public void applyCommand(Tondeuse tondeuse, Pelouse pelouse) throws OutOfPelouseException {
			
			Logger LOG = LoggerFactory.getLogger(Command.class);
			
			// Vérification si manoeuvre possible
			Coordonnee nextCoordonnee = tondeuse.getPosition().nextCoordonnee();
			
			LOG.info("NEXT COORDONNEE : " + nextCoordonnee);
			
			pelouse.isValidCoordonnee(nextCoordonnee);
			
			// Application de la coordonnee
			tondeuse.applyCoordonnee(nextCoordonnee);
		}
	};
	
	/**
	 * Lance la command et modifie donc si possible la position de la tondeuse
	 * si le prochain deplacement n'entrainement pas la sortie de celle ci de la
	 * pelouse
	 * 
	 * @param tondeuse
	 * @param pelouse
	 * @throws OutOfPelouseException
	 */
	public abstract void applyCommand(Tondeuse tondeuse, Pelouse pelouse) throws OutOfPelouseException;
	
	/**
	 * Convertit un string de command en command
	 * Pas de command par defaut
	 * 
	 * @param strCommand
	 * @return
	 * @throws Exception 
	 */
	public static Command getCommand(char strCommand) throws InvalidCommandException {
		for(Command command: EnumSet.allOf(Command.class)){
			if(command.name().equalsIgnoreCase(String.valueOf(strCommand)))
				return command;
		}
		
		throw new InvalidCommandException();
	}
}